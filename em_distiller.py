# Copyright (C) 2021  Ernie Chang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import logging
import time

import torch
import torch.nn as nn
from torch.nn import MSELoss
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.distributions as distributions

from transformers import AdamW
from transformers import BartModel, BartConfig

from distributed_train.distributor import *

logging.basicConfig(level=logging.INFO)
main_logger = logging.getLogger(__name__)

# load data
import sys
import random

source = list(open(sys.argv[1]))
target = list(open(sys.argv[2]))
temp = list(zip(source, target))
random.shuffle(temp)
source, target = zip(*temp)

batch_size = 64

en_text = "show me round trip fares from denver to philadelphia"
fr_text = "Me montrer les tarifs aller-retour de Denver à Philadelphie ."


# define loss function & operators
criterion = torch.nn.KLDivLoss(reduction='sum')
softmax = nn.Softmax(dim=1)


def model_builder(rank,use_dist):


    # define tc model
    from transformers import MarianMTModel, MarianTokenizer
    model_name = "Helsinki-NLP/opus-mt-en-fr"
    tc_model = MarianMTModel.from_pretrained(model_name).to(rank)
    tc_tokenizer = MarianTokenizer.from_pretrained(model_name)

    # define st model
    from transformers import MBartForConditionalGeneration, MBartConfig, MBartTokenizer, PretrainedConfig
    from transformers import BartConfig, BartForConditionalGeneration
    model_name = "facebook/mbart-large-cc25"
    config_path = 'distillers/kd_distillers/bert_config/bart_config.json'
    # big_mbart_config = MBartConfig()
    # st_model = MBartForConditionalGeneration(big_mbart_config).to(rank)
    # configuration = PretrainedConfig.from_json_file(config_path)
    # st_model = BartForConditionalGeneration(configuration).to(rank)
    st_model = MBartForConditionalGeneration.from_pretrained(model_name).to(rank)
    st_tokenizer = MBartTokenizer.from_pretrained(model_name,src_lang="en_XX",tgt_lang="fr_FR")

    if use_dist:
        st_model = DDP(st_model, device_ids=[rank,rank+1],output_device=rank)
        tc_model = DDP(tc_model, device_ids=[rank,rank+1],output_device=rank)
    else:
        st_model = st_model.to(rank)
        tc_model = tc_model.to(rank)

    # build optimizer
    st_optimizer = AdamW(st_model.parameters(), lr=1e-3)
    tc_optimizer = AdamW(tc_model.parameters(), lr=1e-5)

    modules = {
        "st_model": st_model.module if use_dist else st_model,
        "tc_model": tc_model.module if use_dist else tc_model,
        "st_tokenizer": st_tokenizer,
        "tc_tokenizer": tc_tokenizer,
        "st_optimizer": st_optimizer,
        "tc_optimizer": tc_optimizer
    }
    return modules


def generate(rank,model,tokenizer):

    output_translations = ""
    for sample in source[:2]:
        batch = tokenizer.prepare_seq2seq_batch(
            src_texts=[sample],
            src_lang="en_XX",
            return_tensors="pt"
        ).to(rank)
        translated_tokens = model.generate(
            **batch,
            decoder_start_token_id=tokenizer.lang_code_to_id["fr_XX"]
        )
        translation = tokenizer.batch_decode(
            translated_tokens,
            skip_special_tokens=True
        )[0]
        main_logger.info(f"Source: {sample}, Prediction: {translation}")
        output_translations += f"{sample}\t{translation}\n"

    # write to output file
    with open('student.txt','w') as f:
        f.write(f'output_translations {output_translations}')


def batch_iter(src,tgt,batch_size):
    start_index = 0
    while start_index<len(source)-1:
        if start_index+batch_size>len(source)-1:
            yield (src[start_index:-1],tgt[start_index:-1])
        else:
            yield (src[start_index:start_index+batch_size],tgt[start_index:start_index+batch_size])
        start_index += batch_size


def E_STEP(rank,modules,src_batch):
    """
        Require both models to return dictionary containing logits.
    """

    pseudo_target_batch = label_target(rank,modules,src_batch,'st') # label by student model
    batch = modules['tc_tokenizer'].prepare_seq2seq_batch(
        src_batch,src_lang="en_XX",tgt_lang="fr_XX",
        tgt_texts=pseudo_target_batch,return_tensors="pt"
    ).to(rank)

    # disable student model update
    modules['st_model'].requires_grad_(False)

    # compute the loss
    st_logits = modules['st_model'](input_ids=batch['input_ids'],labels=batch['labels']).logits.requires_grad_(False) # forward pass
    tc_logits = modules['tc_model'](input_ids=batch['input_ids'],decoder_input_ids=batch['labels']).logits.requires_grad_(True)  # forward pass
    shaped_st_logits = softmax(torch.max(st_logits,dim=2)[0].view(1,-1))
    shaped_tc_logits = softmax(torch.max(tc_logits,dim=2)[0].detach().clone().resize_(shaped_st_logits.size()).view(1,-1))

    # compute KL loss
    kl_loss = distributions.kl_divergence(
        distributions.Categorical(shaped_tc_logits),
        distributions.Categorical(shaped_st_logits)
    ).requires_grad_(True)

    tc_logits.backward(gradient=kl_loss.repeat(tc_logits.size()))
    modules['tc_optimizer'].step()
    modules['tc_optimizer'].zero_grad()

    return kl_loss.data


def M_STEP(rank,modules,src_batch):
    """
        Require student model to return loss.
    """

    # prepare batch
    pseudo_target_batch = label_target(rank,modules,src_batch,'tc') # label by teacher model
    batch = modules['st_tokenizer'].prepare_seq2seq_batch( # return ?
        src_batch,src_lang="en_XX",tgt_lang="fr_XX",
        tgt_texts=pseudo_target_batch,return_tensors="pt"
    ).to(rank)

    # main_logger.info(f'source: {src_batch[0]} psedu-label: {pseudo_target_batch[0]}')

    # model inference
    modules['st_model'].requires_grad_(True)
    outputs = modules['st_model'](input_ids=batch['input_ids'],labels=batch['labels']) # forward pass
    loss = outputs.loss

    # update
    loss.div(batch_size).backward()
    modules['st_optimizer'].step()
    modules['st_optimizer'].zero_grad()

    return loss.div(batch_size).data


def label_target(rank,modules,src_batch,module_type):
    translated = modules[f'{module_type}_model'].generate(**modules[f'{module_type}_tokenizer'](src_batch, return_tensors="pt", padding=True).to(rank))
    tgt_batch = [modules[f'{module_type}_tokenizer'].decode(t, skip_special_tokens=True) for t in translated]
    del translated
    return tgt_batch


def save_models(path,filename,model,on_device=False):

    if on_device:
        model = torch.quantization.convert(model)
        model = torch.jit.script(model)
        model = torch.utils.optimize_for_mobile(model)
        torch.jit.save(model,f'{path}.{filename}.pth')
    else:
        torch.save(model,f'{path}.{filename}.pth')


def EM_trainer(rank, world_size):

    if world_size>0:
        setup(rank, world_size)

    curr_loss = 0.
    modules = model_builder(rank,use_dist=world_size>0)
    data_loader = batch_iter(source,target,batch_size)
    for i in range(150):

        try:
            (src_batch,tgt_batch) = next(data_loader)
        except:
            data_loader = batch_iter(source,target,1)
            continue

        start_time = time.time()

        if i>1000 and curr_loss<0.02 and False:
            curr_loss = E_STEP(rank=rank,modules=modules,src_batch=src_batch)
        else:
            curr_loss = M_STEP(rank=rank,modules=modules,src_batch=src_batch)

        if i%3==0 and i>1:
            generate(rank,modules['st_model'],modules['st_tokenizer'])
            # translated = modules['tc_model'].generate(**modules['tc_tokenizer']([en_text], return_tensors="pt", padding=True).to(rank))
            # tgt_batch = [modules['tc_tokenizer'].decode(t, skip_special_tokens=True) for t in translated]
            # with open('teacher.txt','a') as f:
            #     f.write(f'{tgt_batch[0]}\n')
            # save_models("mobile","model",modules['tc_model'])

        main_logger.info(f"Step {i}: Latency {time.time()-start_time} Loss {curr_loss}")
        torch.cuda.empty_cache()

    if world_size>0:
        cleanup()


if __name__ == "__main__":
    multi_train(use_dist=False,fn=EM_trainer)
