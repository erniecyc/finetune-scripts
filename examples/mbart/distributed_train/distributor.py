# Copyright (C) 2021  Ernie Chang
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from distributed_train.multiprocess_setup import *


def dist_train(fn, world_size):
    mp.spawn(fn,
             args=(world_size,),
             nprocs=world_size,
             join=True)


def distribute_fn(fn):
    n_gpus = torch.cuda.device_count()
    if n_gpus<4:
        print(f"Requires at least 4 GPUs to run, but got {n_gpus}.")
    else:
        dist_train(fn, 4)


def multi_train(use_dist=False,fn=None):

    # if dist is enabled
    if use_dist:
        distribute_fn(fn)
    else:
        fn(rank=0,world_size=-1)
