
For finetuning of mBART with Fairseq, please refer to the documentation [here](https://gitlab.com/erniecyc/finetune-scripts/-/tree/main/examples/mbart).
The HugginFace based finetuning script is [here](https://gitlab.com/erniecyc/finetune-scripts/-/blob/main/examples/mbart/finetune.py).

Otherwise, refer to mBART finetuning documentation at examples/mbart/README.md

# License

fairseq(-py) is MIT-licensed.
The license applies to the pre-trained models as well.

